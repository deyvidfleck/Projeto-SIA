package model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Aluno extends Usuario{
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "ANO_PRIMEIRA_MATRICULA", length = 100)
	private int anoPrimeiraMatricula; 
	private boolean ativo;
		
	public Aluno() {}

	public Aluno(int anoPrimeiraMatricula, boolean ativo) {
		super();	
		this.anoPrimeiraMatricula = anoPrimeiraMatricula;
		this.ativo = ativo;
	}

	public int getAnoPrimeiraMatricula() {
		return anoPrimeiraMatricula;
	}

	public void setAnoPrimeiraMatricula(int anoPrimeiraMatricula) {
		this.anoPrimeiraMatricula = anoPrimeiraMatricula;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
		
	@OneToOne
	@JoinColumn(name = "id_turma")
	private Turma turma;
	@OneToOne
	@JoinColumn(name = "id_curso")
	private Curso curso;
}