package model;

import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class CursoDao extends AbstractDao<Curso> {

	public boolean save(Curso o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Curso o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Curso o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public Curso find(Curso o) {
		EntityManager em = emf.createEntityManager();
		return em.find(Curso.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<Curso> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + Turma.class.getName()).getResultList();
	}
}
