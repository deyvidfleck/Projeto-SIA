package model;


import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class ProfessorDao extends AbstractDao<Professor>{

	public boolean save(Professor o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Professor o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Professor o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public Professor find(Professor o) {
		EntityManager em = emf.createEntityManager();
		return em.find(Professor.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<Professor> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + Professor.class.getName())
				.getResultList();
	}
}
