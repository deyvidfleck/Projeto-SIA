package model;


import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class TurmaDao extends AbstractDao<Turma>{

	public boolean save(Turma o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Turma o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Turma o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public Turma find(Turma o) {
		EntityManager em = emf.createEntityManager();
		return em.find(Turma.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<Turma> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + Turma.class.getName()).getResultList();
	}
}
