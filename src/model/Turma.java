package model;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.BatchSize;

@Entity
public class Turma {

	@Id
	@Column(name = "id_turma")
	private int idTurma;

	private String horario;
	//private List<String> chamada;

	@ManyToOne
	@JoinColumn(name = "id_professor")
	private Professor professor;
	private String descricao;

	@OneToMany
	@JoinColumn(name = "matricula")
	@BatchSize(size = 15)
	private List<Aluno> alunos;

	@OneToOne
	@JoinColumn(name = "id_disciplina")
	private Disciplina disciplina;

	@OneToMany
	@JoinColumn(name = "id_prova")
	private Collection<Prova> provas;
}