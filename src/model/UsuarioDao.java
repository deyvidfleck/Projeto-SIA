package model;

import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class UsuarioDao extends AbstractDao<Usuario> {

	public boolean save(Usuario o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Usuario o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Usuario o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public TAE find(Usuario o) {
		EntityManager em = emf.createEntityManager();
		return em.find(TAE.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + TAE.class.getName()).getResultList();
	}
}
