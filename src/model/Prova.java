package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Prova {
	@Id
	@Column(name = "id_prova")
	private int idprova;
	private String prova;

	public int getIdprova() {
		return idprova;
	}

	public void setIdprova(int idprova) {
		this.idprova = idprova;
	}

	public String getProva() {
		return prova;
	}

	public void setProva(String prova) {
		this.prova = prova;
	}
	@ManyToOne
	@JoinColumn(name = "matricula")
	private Aluno aluno;
	
	@ManyToOne
	@JoinColumn(name = "id_disciplina")
	private Disciplina disciplina;
}
