package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Professor extends Usuario {
	private String area;
	private double salario;
	@Column(name="data_ingresso")
	private String data_ingresso;
	private String lattes;
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public String getData_ingresso() {
		return data_ingresso;
	}
	public void setData_ingresso(String data_ingresso) {
		this.data_ingresso = data_ingresso;
	}
	public String getLattes() {
		return lattes;
	}
	public void setLattes(String lattes) {
		this.lattes = lattes;
	}
	
}
