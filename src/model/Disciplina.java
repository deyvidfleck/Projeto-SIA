package model;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Disciplina {

	@Id
	@Column(name = "ID_DISCIPLINA")
	private int idDisciplina;
	private String nome;
	@OneToOne
	@JoinColumn(name = "id_professor")
	private Professor professor;
	private String ementa;
	private String objetivos;
	private String c_programatico;
	private String referencias;
	@OneToMany
	@JoinColumn(name = "id_curso")
	private List<Curso> curso;

	public int getIdDisciplina() {
		return idDisciplina;
	}

	public void setIdDisciplina(int idDisciplina) {
		this.idDisciplina = idDisciplina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmenta() {
		return ementa;
	}

	public void setEmenta(String ementa) {
		this.ementa = ementa;
	}

	public String getObjetivos() {
		return objetivos;
	}

	public void setObjetivos(String objetivos) {
		this.objetivos = objetivos;
	}

	public String getC_programatico() {
		return c_programatico;
	}

	public void setC_programatico(String c_programatico) {
		this.c_programatico = c_programatico;
	}

	public String getReferencias() {
		return referencias;
	}

	public void setReferencias(String referencias) {
		this.referencias = referencias;
	}

}
