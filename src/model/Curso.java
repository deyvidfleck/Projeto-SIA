package model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Curso {

	@Id
	@Column(name = "ID_CURSO")
	private int idCurso;
	private String nome;
	private int duracao;
	private String descricao;
	@OneToMany
	@Column(name = "id_disciplina")
	private Collection<Disciplina> disciplinas;

	public int getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

// CREATE TABLE Curso(
// id_curso int primary key
// nome varchar2(20),
// duracao number,
// descricao varchar);
