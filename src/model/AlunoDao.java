package model;

import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class AlunoDao extends AbstractDao<Aluno> {
	
	public boolean save(Aluno o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Aluno o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Aluno o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public Aluno find(Aluno matricula) {
		EntityManager em = emf.createEntityManager();
		return em.find(Aluno.class, matricula);
	}

	@SuppressWarnings("unchecked")
	public List<Aluno> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + Turma.class.getName()).getResultList();
	}
}
