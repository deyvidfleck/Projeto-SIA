package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TAE extends Usuario {
	private String setor;
	private double salario;
	
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}

}
