package model;

import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class TAEDao extends AbstractDao<TAE> {

	public boolean save(TAE o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(TAE o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(TAE o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public TAE find(TAE o) {
		EntityManager em = emf.createEntityManager();
		return em.find(TAE.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<TAE> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + TAE.class.getName()).getResultList();
	}
}
