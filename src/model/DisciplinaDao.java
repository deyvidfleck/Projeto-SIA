package model;

import java.util.List;

import javax.persistence.EntityManager;

import javafx.collections.ObservableList;

public class DisciplinaDao extends AbstractDao<Disciplina> {

	public boolean save(Disciplina o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();
		return false;
	}

	@Override
	public int update(Disciplina o) {
		this.save(o);
		return 0;
	}

	@Override
	public int remove(Disciplina o) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();
		em.close();
		return 0;
	}

	@Override
	public Disciplina find(Disciplina o) {
		EntityManager em = emf.createEntityManager();
		return em.find(Disciplina.class, o);
	}

	@SuppressWarnings("unchecked")
	public List<Disciplina> findAll() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("FROM " + Turma.class.getName()).getResultList();
	}
}
