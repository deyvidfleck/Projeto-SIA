package model;

import java.sql.Connection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javafx.collections.ObservableList;

public abstract class AbstractDao<T> {
	protected static EntityManagerFactory emf;

	public abstract boolean save(T o);

	public abstract int update(T o);

	public abstract int remove(T o);

	public abstract T find(T o);

	public abstract List<T> findAll();

}
