package principal;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import controller.LoginController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.application.Application;
		

public class Aplicacao extends Application {	
	
	public static Stage stage;

	/**
	 * Iniciou o programa
	 */
	@Override
	public void start(Stage primaryStage) {
		stage = primaryStage;
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("../view/TelaLogin.fxml"));
			AnchorPane root = (AnchorPane) loader.load();

			LoginController lc = loader.getController();
			lc.setStage(stage);

			Scene scene = new Scene(root, 600, 400);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Stage getStage(){
		return stage; 
	}	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistence");
		EntityManager entityManager = emf.createEntityManager();
		// do something
		entityManager.close();	
		launch(args);
	}	
}

