package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Usuario;

public class LoginController {
	private Stage st;
	
	@FXML
	private TextField txtMatricula;
	
	@FXML
	private PasswordField txtSenha;
	
	@FXML
	private ToggleGroup tipoacesso;
	
	public void initialize(URL Location, ResourceBundle resource){
		//TODO Auto-generated method stub
	}
	
	public void setStage(Stage st){
		this.st = st;
	}
	
	
	
	@FXML
	private void Login() throws IOException{
		
		Usuario user = new Usuario();
		user.setMatricula(txtMatricula.getText());
		user.setSenha(txtSenha.getText());
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("view/TelaMenu.fxml"));
	
		AnchorPane root = (AnchorPane) loader.load();
		Scene scene = new Scene(root,600,400);
		st.setScene(scene);
		st.show();
	}	
}
