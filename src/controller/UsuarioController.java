package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Usuario;
import model.UsuarioDao;
import javafx.scene.image.ImageView;

public class UsuarioController{
	
	private Stage st;
	
	@FXML
	private ImageView myImageView;
	
	@FXML
	private TextField txtNome, txtEmail, txtMatricula, txtSenha, txtEndereco;
	
	@FXML
	private ToggleGroup tipoUsuario;
	
	@FXML
	private ToggleGroup verificaAtivo;
	
	@FXML
	private void abrirArquivo(){
		FileChooser fileChooser = new FileChooser();
		File file = fileChooser.showOpenDialog(st);
		if (file != null) {
			BufferedImage bufferedImage;
			try {
				bufferedImage = ImageIO.read(file);
				Image image = SwingFXUtils.toFXImage(bufferedImage, null);
				myImageView.setImage(image);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	@FXML
	private void salvarUsuario(){
		Usuario user = new Usuario();
		UsuarioDao userdao = new UsuarioDao();
	}
	
	@FXML
	private void editarUsuario(){
		
	}
	
	@FXML
	private void excluirUsuario(){
		
	}
}
